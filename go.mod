module gitlab.com/VagueCoder0to.n/Timesheet-Reminder

go 1.13

require (
	github.com/go-redis/redis/v8 v8.8.3
	github.com/go-telegram-bot-api/telegram-bot-api v4.6.4+incompatible
	github.com/stretchr/testify v1.7.0
	github.com/technoweenie/multipartstreamer v1.0.1 // indirect
)
