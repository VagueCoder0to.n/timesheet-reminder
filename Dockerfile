FROM golang:1.15.6-alpine3.12

ENV PROJECT_ROOT=$GOPATH/src/gitlab.com/VagueCoder0to.n/Timesheet-Reminder
ENV ENV GOBIN $GOPATH/bin
ENV PATH=$PATH:$GOBIN

WORKDIR $PROJECT_ROOT

COPY . .

RUN apk add --no-cache \
    gcc build-base musl-dev
# General prerequisites
# make git \
# GCC compiler related
# Libtool should help in package building in GCC.
# libtool \
# To work with certs
# ca-certificates \
# 
# dumb-init 

RUN mkdir -p $PROJECT_ROOT

RUN go mod download

RUN go build -o=$GOBIN/timesheet-reminder $PROJECT_ROOT/app;

CMD timesheet-reminder
