package json_test

import (
	"bytes"
	"regexp"
	"testing"

	"github.com/stretchr/testify/assert"

	jsonPkg "gitlab.com/VagueCoder0to.n/Timesheet-Reminder/app/json"
)

// TestEncode has tests for Encode function
func TestEncode(t *testing.T) {

	// To read the logs back to memory
	outputWriter := &bytes.Buffer{}
	r := &jsonPkg.Record{
		User:    "User-1",
		Message: "Test Message",
		Date:    jsonPkg.UnixTime(1620849052),
	}

	// Checks whether encoding is returning any error
	t.Run("Encoding", func(t *testing.T) {
		err := jsonPkg.Encode(outputWriter, r)
		if err != nil {
			assert.Fail(t, "Encode shouldn't return an error. Received: %v", err)
		}
	})

	// Checks the JSON keys in encoded data
	t.Run("JSON-Keys", func(t *testing.T) {
		assert.Contains(t, string(outputWriter.Bytes()), "user", "Encoded bytes of JSON should have key %q in it.", "user")
		assert.Contains(t, string(outputWriter.Bytes()), "message", "Encoded bytes of JSON should have key %q in it.", "message")
		assert.Contains(t, string(outputWriter.Bytes()), "date", "Encoded bytes of JSON should have key %q in it.", "date")
	})

	// Checks the marshalled values in encoded data
	t.Run("MarshalledValues", func(t *testing.T) {
		assert.Contains(t, string(outputWriter.Bytes()), "User-1", "Encoded bytes of JSON should have value %q in it.", "User-1")
		assert.Contains(t, string(outputWriter.Bytes()), "Test Message", "Encoded bytes of JSON should have value %q in it.", "Test Message")

		pattern := regexp.MustCompile(`[A-Z][a-z]{2}, \d{2}-[A-Z][a-z]{2}-\d{4} \d{2}:\d{2}:\d{2} [A-Z]{3}`)
		assert.Regexp(t, pattern, string(outputWriter.Bytes()), "Encoded bytes of JSON should have date matching the pattern.")
	})
}
