package json

import (
	"encoding/json"
	"io"
	"time"
)

type UnixTime int

type Record struct {
	User    string   `json:"user"`
	Message string   `json:"message"`
	Date    UnixTime `json:"date"`
}

func (u *UnixTime) MarshalJSON() ([]byte, error) {
	var date time.Time
	date = time.Unix(int64(*u), 0)
	return []byte(date.Format("\"Mon, 02-Jan-2006 15:04:05 MST\"")), nil
}

func Encode(w io.Writer, r *Record) error {
	return json.NewEncoder(w).Encode(r)
}
