package logger

import (
	"io"
	"log"
)

type Loggers struct {
	Err *log.Logger
	Out *log.Logger
}

func NewLoggers(out, err io.Writer) *Loggers {
	return &Loggers{
		Err: log.New(err, "[Timesheet-Reminder-Bot Error] ", log.Lshortfile|log.LstdFlags),
		Out: log.New(out, "[Timesheet-Reminder-Bot Output] ", log.Lshortfile|log.LstdFlags),
	}
}
