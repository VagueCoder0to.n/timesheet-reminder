package logger_test

import (
	"bytes"
	"testing"

	"github.com/stretchr/testify/assert"

	loggerPkg "gitlab.com/VagueCoder0to.n/Timesheet-Reminder/app/logger"
)

// TestNewLoggers has tests for NewLoggers function
func TestNewLoggers(t *testing.T) {

	// To read the logs back to memory
	err := &bytes.Buffer{}
	out := &bytes.Buffer{}
	l := loggerPkg.NewLoggers(out, err)

	// Sample logs
	l.Err.Println("Error Logs")
	l.Out.Println("Output Logs")

	// Check whether printed comment is being logged correctly
	t.Run("Logs", func(t *testing.T) {
		assert.Contains(t, string(err.Bytes()), "Error Logs", "Logs should have the error %q as per test case.", "Error Logs")
		assert.Contains(t, string(out.Bytes()), "Output Logs", "Logs should have the output %q as per test case.", "Output Logs")
	})

	// Check whether prefix is printed correctly
	t.Run("Prefix", func(t *testing.T) {
		assert.Contains(t, string(err.Bytes()), "Error Logs", "Error logs should have prefix %q as per test case.", "[Timesheet-Reminder-Bot Error]")
		assert.Contains(t, string(out.Bytes()), "Output Logs", "Output logs should have prefix %q as per test case.", "[Timesheet-Reminder-Bot Output]")
	})
}
