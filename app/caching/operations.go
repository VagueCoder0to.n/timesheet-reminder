package caching

import (
	"fmt"

	redis "github.com/go-redis/redis/v8"
)

var (
	err    error
	keys   []string
	val    interface{}
	pong   string
	status *redis.StatusCmd
	cmd    *redis.IntCmd
)

// Set inserts/updates key-values pair in db.
func (r *RedisClient) Set(key string, val interface{}) error {
	err = r.Client.Set(r.Ctx, key, val, 0).Err()
	if err != nil {
		return err
	}
	return nil
}

// Get checks whether key is in db. If yes, then it returns it's corresponding value.
func (r *RedisClient) Get(key string) (interface{}, error) {
	val, err = r.Client.Get(r.Ctx, key).Result()
	if err != nil {
		if err == redis.Nil {
			return nil, fmt.Errorf("key error: key %q not found in database", key)
		} else {
			return nil, err
		}
	}
	return val, nil
}

// DeleteKey deletes a key (with value) from db
func (r *RedisClient) DeleteKey(key string) bool {
	cmd = r.Client.Del(r.Ctx, key)

	// Validate whether the key is deleted
	return cmd.Val() != 0
}

// CountKeys returns total number of keys
func (r *RedisClient) CountKeys() (int, error) {
	keys, err = r.Client.Keys(r.Ctx, "*").Result()
	if err != nil {
		return -1, nil
	}
	return len(keys), nil
}

// KeysStartingWith returns slice of keys starting with given pattern
func (r *RedisClient) KeysStartingWith(start string) ([]string, error) {
	keys, err = r.Client.Keys(r.Ctx, start+"*").Result()
	if err != nil {
		return nil, err
	}

	if len(keys) == 0 {
		return nil, fmt.Errorf("no keys found that start with %q", start)
	}
	return keys, nil
}

// FlushDB deletes all the keys in current database
func (r *RedisClient) FlushDB() bool {
	status = r.Client.FlushDB(r.Ctx)
	return status.Err() == nil
}

// FlushAll deletes all the keys in all databases
func (r *RedisClient) FlushAll() bool {
	status = r.Client.FlushAll(r.Ctx)
	return status.Err() == nil
}
