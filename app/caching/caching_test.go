package caching_test

import (
	"strconv"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/VagueCoder0to.n/Timesheet-Reminder/app/caching"
)

// Local variables used by one or more sub-test cases
var (
	client             *caching.RedisClient
	key, testKey, pong string
	val, testVal       interface{}
	keys               []string
	count              int
	flag               bool
	err                error
	db                 int = -1
)

func TestPing(t *testing.T) {

	// To test Ping
	pong, err = caching.Ping()
	assert.Nil(t, err, "Ping shouldn't return error")
	assert.Equal(t, "PONG", pong, "Ping should return PONG")
}

func TestRedisClient(t *testing.T) {

	// Tests on DatabaseNumber()
	t.Run("DatabaseNumber", func(t *testing.T) {
		db, err = caching.DatabaseNumber()
		assert.Nil(t, err, "redis databases shouldn't be already full")
		assert.GreaterOrEqual(t, db, 0, "db number should be greater or equal to 0")
		assert.Less(t, db, 16, "db number should be less than 16")
	})

	// Tests on NewRedisClient()
	t.Run("Init", func(t *testing.T) {
		client, err = caching.NewRedisClient()
		assert.Nil(t, err, "redis databases shouldn't be full")
	})

	// Tests on Set()
	t.Run("Set", func(t *testing.T) {
		key, val = "TestKey", 123
		client.Set(key, val)

		count, err = caching.DatabaseNumber()
		if db == 15 {
			assert.NotNil(t, err, "adding 1 db to 15 should throw overflow error")
		} else {
			assert.Nil(t, err, "adding 1 db to %d shouldn't throw overflow error", db)
		}

		assert.Equal(t, count, db+1, "total dbs after adding 1 to %d should be %d, not %d", db, db+1, count)
	})

	// Tests on Get()
	t.Run("Get", func(t *testing.T) {

		// Valid Case
		testVal, err = client.Get(key)

		testVal, err = strconv.Atoi(testVal.(string))
		assert.Nil(t, err, "value %q should parsable to integer", key)
		assert.Nil(t, err, "key %q should be in db", key)
		assert.Equal(t, val, testVal, "value for key %q should be %q, not %q", key, val, testVal)

		// Invalid Case
		testKey = "Something"
		_, err = client.Get(testKey)
		assert.NotNil(t, err, "Get(%q) should through error as key %q isn't inserted yet while testing", testKey, testKey)
	})

	// Tests on CountKeys()
	t.Run("CountKeys", func(t *testing.T) {
		count, err = client.CountKeys()
		assert.Nil(t, err, "CountKeys shouldn't throw error")
		assert.Equal(t, 1, count, "CountKeys should return 1 as only 1 key was inserted in testcase")
	})

	// Test on KeysStartingWith()
	t.Run("KeysStartingWith", func(t *testing.T) {

		testKey = "Test"
		// Valid Case
		keys, err = client.KeysStartingWith(testKey)
		assert.Nil(t, err, "KeysStartingWith shouldn't throw error for keys starting with %q", testKey)
		assert.Contains(t, keys, key, "Keys starting with pattern %q should contain %q", testKey, key)

		testKey = "Unknown"
		keys, err = client.KeysStartingWith(testKey)
		assert.NotNil(t, err, "KeysStartingWith should throw error as no keys starting with %q as per test case", testKey)
		assert.Nil(t, keys, "keys should be nil as there shouldn't be any keys starting with %q in test case", testKey)
	})

	// Tests on DeleteKey()
	t.Run("DeleteKey", func(t *testing.T) {

		err = client.Set(testKey, testVal)
		assert.Nil(t, err, "Set(%q) shouldn't throw error: %v", err)

		flag = client.DeleteKey(testKey)
		assert.True(t, flag, "Since key %q is already inserted, DeleteKey(%q) should return true")

	})

	// Tests on FlushDB()
	t.Run("FlushDB", func(t *testing.T) {

		// Before flushing keys
		t.Run("CountKeysBefore", func(t *testing.T) {
			count, err = client.CountKeys()
			assert.Nil(t, err, "CountKeys shouldn't throw error")
			assert.Equal(t, 1, count, "CountKeys should return 1 as only 1 key was inserted in testcase")
		})

		// Flushing keys
		t.Run("FlushKeys", func(t *testing.T) {
			flag = client.FlushDB()
			assert.True(t, flag, "Flushing should return true, i.e., success")
		})

		// After flushing keys
		t.Run("CountKeysAfter", func(t *testing.T) {
			count, err = client.CountKeys()
			assert.Nil(t, err, "CountKeys shouldn't throw error")
			assert.Equal(t, 0, count, "CountKeys should return 0 as flushed")
		})
	})

	// Tests on FlushAll
	t.Run("FlushAll", func(t *testing.T) {

		// Before flushing DBs
		t.Run("CountDBsBefore", func(t *testing.T) {
			db, err = caching.DatabaseNumber()
			if err == nil && db == -1 {
				assert.FailNow(t, "Invalid db", "Database Number should either be overflowing or [0, 16), not -1")
			}
		})

		// Flushing DBs
		t.Run("FlushDBs", func(t *testing.T) {
			flag = client.FlushAll()
			assert.True(t, flag, "FlushAll should return true, i.e., success")
		})

		// After flushing DBs
		t.Run("CountDBsAfter", func(t *testing.T) {
			db, err = caching.DatabaseNumber()
			assert.Nil(t, err, "redis databases shouldn't be full after FlushAll")
			assert.Equal(t, 0, db, "Database Number should return 0 after FlushAll")
		})
	})
}
