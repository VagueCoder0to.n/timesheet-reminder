package config

import (
	"os"
)

var (
	ProjectRoot   string
	Token         string
	RedisAddr     string
	RedisPassword string
)

func init() {
	ProjectRoot = os.Getenv("PROJECT_ROOT")
	Token = os.Getenv("BOT_TOKEN")
	RedisAddr = os.Getenv("REDIS_ADDRESS")
	RedisPassword = os.Getenv("REDIS_PASSWORD")
}
