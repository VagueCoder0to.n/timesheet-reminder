package config_test

import (
	"testing"

	"github.com/stretchr/testify/assert"

	configPkg "gitlab.com/VagueCoder0to.n/Timesheet-Reminder/app/config"
)

func TestVariables(t *testing.T) {
	assert.NotEmpty(t, configPkg.ProjectRoot, "Env PROJECT_ROOT is not found.")
	assert.NotEmpty(t, configPkg.Token, "Env BOT_TOKEN is not found.")
}
