package stickers

import (
	"fmt"
	"math/rand"
	"time"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"gitlab.com/VagueCoder0to.n/Timesheet-Reminder/app/caching"
)

// Variables
var (

	// Unexported variables
	ok             bool
	idx            int
	err            error
	id, key, start string
	keys           []string
	temp           interface{}
	stickerFile    *tgbotapi.Sticker
)

// Sticker is a custom data type that holds Telegram bot API's Sticker type.
type sticker struct {
	file *tgbotapi.Sticker
}

// StickerClient is a custom data type that holds Redis Client.
type StickerClient struct {

	// Unexported fields
	client *caching.RedisClient
}

// init initiates before all
func init() {

	// To generate random values henceforth in the session
	rand.Seed(time.Now().UnixNano())
}

// NewSticker initiates new Sticker and returns reference.
// func NewSticker(sticker tgbotapi.Sticker) *sticker {
// 	return &sticker{
// 		file: &sticker,
// 	}
// }

// NewStickerClient initiates new StickerClient and returns reference.
func NewStickerClient() (*StickerClient, error) {
	var redisClient *caching.RedisClient
	redisClient, err = caching.NewRedisClient()
	if err != nil {
		return nil, fmt.Errorf("error in NewStickerClient(): %w", err)
	}

	return &StickerClient{
		client: redisClient,
	}, nil
}

// GetStickerFileID just returns the FileID of the sticker which is inside the unexported field.
func (st *sticker) GetStickerFileID() string {
	return st.file.FileID
}

// AddSticker checks whether sticker already exists, and adds if not.
func (s *StickerClient) AddSticker(sticker tgbotapi.Sticker) (bool, error) {
	id = sticker.FileID
	start = string(id[0])
	keys, err = s.client.KeysStartingWith(start)
	if err != nil {
		return false, fmt.Errorf("error in AddSticker(): %w", err)
	}

	for _, key = range keys {
		if key == sticker.FileID {
			return false, nil
		}
	}

	if err = s.client.Set(id, sticker); err != nil {
		return false, fmt.Errorf("error in AddSticker(): %w", err)
	}

	return true, nil
}

// randomStickerID is returns random sticker-ID from Redis DB. Unexported.
func (s *StickerClient) randomStickerID() (string, error) {
	keys, err = s.client.KeysStartingWith("")
	if err != nil {
		return "", fmt.Errorf("error in randomKeyID(): %w", err)
	}

	idx = rand.Intn(len(keys))
	return keys[idx], nil
}

// RandomSticker fetches random Sticker ID from Redis DB, encapsulates and returns as Sticker type.
func (s *StickerClient) RandomSticker() (*sticker, error) {
	id, err = s.randomStickerID()
	if err != nil {
		return nil, fmt.Errorf("error in RandomSticker(): %w", err)
	}

	temp, err = s.client.Get(id)
	if err != nil {
		return nil, fmt.Errorf("error in RandomSticker(): %w", err)
	}
	stickerFile, ok = temp.(*tgbotapi.Sticker)
	if !ok {
		if err != nil {
			return nil, fmt.Errorf("error in RandomSticker(): tgbotapi.Sticker parsing failed")
		}
	}

	return &sticker{
		file: stickerFile,
	}, nil
}
