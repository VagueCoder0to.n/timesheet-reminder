package bot

import (
	"fmt"
	"os"

	"gitlab.com/VagueCoder0to.n/Timesheet-Reminder/app/json"
	"gitlab.com/VagueCoder0to.n/Timesheet-Reminder/app/logger"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
)

var err error

func RunBot(log *logger.Loggers, token string) {
	bot, err := tgbotapi.NewBotAPI(token)
	if err != nil {
		log.Err.Fatalf("Bot Creation Error: %v", err)
	}

	bot.Debug = false

	log.Out.Printf("Authorized on account %s", bot.Self.UserName)

	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	updates, err := bot.GetUpdatesChan(u)

	for update := range updates {

		if update.Message == nil {
			// ignore any non-Message Updates
			continue
		}

		fmt.Printf("%+v\n", update.Message)
		if update.Message.Chat != nil {
			fmt.Printf("\n Chat: %+v\n", update.Message.Chat)
		}
		if update.Message.Sticker != nil {
			fmt.Printf("\n Sticker: %+v\n", update.Message.Sticker)
			sticker := tgbotapi.NewStickerShare(update.Message.Chat.ID, update.Message.Sticker.FileID)
			sticker.ReplyToMessageID = update.Message.MessageID
			bot.Send(sticker)
		}

		err = json.Encode(os.Stdout, &json.Record{
			User:    update.Message.From.UserName,
			Message: update.Message.Text,
			Date:    json.UnixTime(update.Message.Date),
		})

		if err != nil {
			log.Err.Fatalf("JSON Marshal Error: %v", err)
		}

		msg := tgbotapi.NewMessage(update.Message.Chat.ID, update.Message.Text)
		msg.ReplyToMessageID = update.Message.MessageID

		bot.Send(msg)
	}
}

/*

{
	"ok": true,
	"result": {
		"message_id": 25,
		"from": {
			"id": 1863786503,
			"is_bot": true,
			"first_name": "TimeSheetBot",
			"username": "TimesheetReminder_bot"
			},
		"chat": {
			"id": 1010067444,
			"first_name": "Vague",
			"last_name": "Coder",
			"username": "Din_Djarin001",
			"type":"private"
			},
		"date": 1620849066,
		"reply_to_message": {
			"message_id": 24,
			"from": {
				"id": 1010067444,
				"is_bot": false,
				"first_name": "Vague",
				"last_name": "Coder",
				"username": "Din_Djarin001",
				"language_code": "en"
				},
			"chat": {
				"id": 1010067444,
				"first_name": "Vague",
				"last_name": "Coder",
				"username": "Din_Djarin001",
				"type": "private"
				},
			"date": 1620849052,
			"text": "yo"
			},
		"text":"yo"
	}
}
*/
