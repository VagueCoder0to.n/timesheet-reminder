package main

import (
	"os"

	"gitlab.com/VagueCoder0to.n/Timesheet-Reminder/app/bot"
	"gitlab.com/VagueCoder0to.n/Timesheet-Reminder/app/config"
	"gitlab.com/VagueCoder0to.n/Timesheet-Reminder/app/logger"
)

func main() {

	// Custom loggers. Separate for standard output and error
	logger := logger.NewLoggers(os.Stdout, os.Stderr)

	// Bot Token from configuration file (app/config/env.sh)
	token := config.Token

	// Running bot
	bot.RunBot(logger, token)
}
